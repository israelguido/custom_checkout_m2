<?php


namespace Custom\Checkout\Plugin;

use Magento\Checkout\Block\Checkout\LayoutProcessor;


class AddressLayoutProcessor
{
    /**
     * @param LayoutProcessor $subject
     * @param array $jsLayout
     * @return array
     */
    public function afterProcess(LayoutProcessor $subject, array $jsLayout)
    {
        /*$fieldGroup = &$jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']
        ['children']['shippingAddress']['children']['shipping-address-fieldset']
        ['children']['custom-field-group']['children']['field-group']['children'];*/

        $fieldGroup = &$jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']
        ['children']['billing-address-form']['children']['form-fields']['children']
        ['custom-field-group']['children']['field-group']['children'];

        $billingAddressFields = &$jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']
        ['children']['billing-address-form']['children']['form-fields']['children'];

        $fieldGroup['country_id'] = $billingAddressFields['country_id'];
        $fieldGroup['postcode'] = $billingAddressFields['postcode'];

        $billingAddressFields['country_id']['visible'] = false;
        $billingAddressFields['postcode']['visible'] = false;

        return $jsLayout;
    }
}